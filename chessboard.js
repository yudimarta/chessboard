const makeChessboard = () => {
  let chessboard = []
  let temp = []
  let word = ''
  let selainPion = ['B', 'K', 'M', 'RT', 'RJ', 'M', 'K', 'B']
  let pion = 'P'

  for(i=0;i<=7;i++){
    if(i==0){
      for(j=0;j <= selainPion.length-1; j++){
        word = selainPion[j] + ' Black'
        temp.push(word)
      }
      chessboard.push(temp)
      //console.log(chessboard)
    }else if(i==1){
      temp = []
      word = ''
      for(k=0;k<=7;k++){
        word = pion + ' Black'
        temp.push(word)
      }
      chessboard.push(temp)
    } else if(i<=5){
      temp = []
      for(l=0;l<=7;l++){
        temp.push('-')
      }
      chessboard.push(temp)
    } else if(i==6){
      temp = []
      word = ''
      for(k=0;k<=7;k++){
        word = pion + ' White'
        temp.push(word)
      }
      chessboard.push(temp)
    } else {
      temp = []
      word = ''
      for(m=0;m <= selainPion.length-1; m++){
        word = selainPion[m] + ' White'
        temp.push(word)
      }
      chessboard.push(temp)
      //console.log(chessboard)
    }
  }
  

  return chessboard
}

const printBoard = x => {
  console.log(x)
}

printBoard(makeChessboard())
